from flask import Flask, request, jsonify
import paho.mqtt.client as mqtt

server= Flask(__name__)
client= mqtt.Client("publisher")

@server.route('/temperature', methods=['POST'])
def temperature():
    temp= float(request.form.get('temperature'))
    client.connect("localhost")
    if temp>= 30.0:
        d= {
            "status": "200 OK",
            "fan": "ON"
        }
        client.publish("home/fan", "ON")
    else:
        d= {
            "status": "200 OK",
            "Fan": "OFF"
        }
        client.publish("home/fan", "OFF")
    client.disconnect()

    return jsonify(d)
server.run(port=3000, debug=True)